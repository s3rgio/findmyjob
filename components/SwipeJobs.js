'use strict';


import React, {Component} from 'react';

import {StyleSheet, Text, View, Image, TouchableNativeFeedback} from 'react-native';

import SwipeCards from 'react-native-swipe-cards';

let Card = React.createClass({


  render() {

    return (

      <View style={styles.card}>
        <Image style={styles.thumbnail} source={{uri: this.props.image}} />
        <Text style={styles.text}>Company #{this.props.name}</Text>
      </View>

    )
  }

})


let NoMoreCards = React.createClass({
  render() {
    return (
      <View style={styles.noMoreCards}>
        <Text>No more cards</Text>
      </View>
    )
  }
})

const Cards = [
  {name: '1', image: 'http://betanews.com/wp-content/uploads/2012/05/ibm-logo.jpeg'},
  {name: '2', image: 'http://cdn.ndtv.com/tech/images/gadgets/microsoft-white-635.jpg'},
  {name: '3', image: 'http://www.v3b.com/wp-content/uploads/2012/09/LinkedInBuilding.jpg'},
  {name: '4', image: 'http://a.fastcompany.net/multisite_files/fastcompany/imagecache/1280/poster/2016/01/3055560-poster-what-the-interview-process-is-like-at-google-apple-amazon-and-other-tech-companies.jpg'},
  {name: '5', image: 'http://image.slidesharecdn.com/appleincpresentatioinslideshare-131122014841-phpapp02/95/apple-inc-presentatioin-1-638.jpg?cb=1395846560'},
  {name: '6', image: 'https://pbs.twimg.com/profile_images/708322719717269504/QZNIjMnd.jpg'},
  {name: '7', image: 'http://www.arabianbusiness.com/incoming/article585338.ece/BINARY/abn_amro.jpg'},
  {name: '8', image: 'http://www.amplexcorp.com/images/Carousel/Cisco-logo-website2.png'},
  {name: '9', image: 'http://www.underconsideration.com/brandnew/archives/yahoo_logo_detail.png'},
]

const Cards2 = [
  {name: '10', image: 'https://media.giphy.com/media/12b3E4U9aSndxC/giphy.gif'},
  {name: '11', image: 'https://media4.giphy.com/media/6csVEPEmHWhWg/200.gif'},
  {name: '12', image: 'https://media4.giphy.com/media/AA69fOAMCPa4o/200.gif'},
  {name: '13', image: 'https://media.giphy.com/media/OVHFny0I7njuU/giphy.gif'},
]

export default React.createClass({
  getInitialState() {
    return {
      cards: Cards,
      outOfCards: false
    }
  },
  handleYup (card) {
    console.log("Interesting")
  },
  handleNope (card) {
    console.log("Discard")
  },
  cardRemoved (index) {
    console.log(`The index is ${index}`);

    let CARD_REFRESH_LIMIT = 3

    if (this.state.cards.length - index <= CARD_REFRESH_LIMIT + 1) {
      console.log(`There are only ${this.state.cards.length - index - 1} cards left.`);

      if (!this.state.outOfCards) {
        console.log(`Adding ${Cards2.length} more cards`)

        this.setState({
          cards: this.state.cards.concat(Cards2),
          outOfCards: true
        })
      }

    }

  },
  render() {
    return (
      <SwipeCards
        cards={this.state.cards}
        loop={false}

        renderCard={(cardData) => <Card {...cardData} />}
        renderNoMoreCards={() => <NoMoreCards />}
        showYup={true}
        showNope={true}

        handleYup={this.handleYup}
        handleNope={this.handleNope}
        cardRemoved={this.cardRemoved}
      />
    )
  }
})

const styles = StyleSheet.create({
  card: {
    alignItems: 'center',
    borderRadius: 5,
    overflow: 'hidden',
    borderColor: 'grey',
    backgroundColor: 'white',
    borderWidth: 1,
    elevation: 1,
  },
  thumbnail: {
    flex: 1,
    width: 300,
    height: 300,
  },
  text: {
    fontSize: 20,
    paddingTop: 10,
    paddingBottom: 10
  },
  noMoreCards: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
})
