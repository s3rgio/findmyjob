import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image
} from 'react-native';

export default class AboutUs extends Component {
    render() {
        return (
            <View style={styles.body}>

                <Text style={styles.text}>
                    Come and visit us to and find your job!
                </Text>

                <Image
                    source={require('./logo.png')}
                    resizeMode="contain"
                    style={styles.img}
                />

            </View>
        )
    }
}

const styles = StyleSheet.create({
    body: {
        flex: 4,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        backgroundColor: 'whitesmoke',
        alignItems: 'center'
    },
    smalltext: {
        fontSize: 10
    },
    text: {
        marginTop: 15,
        fontSize: 15
    },
    img: {
        width: 250
    }
});