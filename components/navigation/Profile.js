import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView
} from 'react-native';

export default class Profile extends Component {
    render() {
        return (
            <View>
                <View style={styles.body}>
                    <View style={styles.top}>
                        <View style={styles.textBox}>
                            <Text style={styles.name}> Steve</Text>
                            <Text> City: Silicon Valley</Text>
                            <Text> Phone: 123456789</Text>
                        </View>
                        <Image
                            source={require('./jobs.jpg')}
                            resizeMode="cover"
                            style={styles.image} />
                    </View>
                    <View style={styles.bottom}>
                        <Text style={styles.title}> Profile </Text>
                        <Text style={styles.basicText}> Donut jujubes powder bear claw bonbon tootsie roll apple pie chocolate bar chocolate. Ice cream cake I love bonbon. Candy chocolate cake I love pie gummi bears apple pie lollipop. Carrot cake fruitcake sweet.
                            Macaroon marzipan wafer I love chupa chups marzipan marshmallow marshmallow sesame snaps. Cake muffin bear claw gummi bears fruitcake biscuit carrot cake jelly. Cake fruitcake caramels lollipop. Fruitcake tiramisu icing pudding candy. </Text>
                        <Text style={styles.title}> Work Experience </Text>
                        <Text style={styles.basicText}> Wafer I love tootsie roll jujubes caramels caramels pudding bonbon. Fruitcake I love macaroon chupa chups I love lemon drops chupa chups chocolate cake. Cotton candy chocolate chocolate bar icing cheesecake. Cotton candy jelly-o brownie muffin I love.
                            Caramels croissant cheesecake I love dessert sweet carrot cake jelly beans topping. Fruitcake I love macaroon chupa chups I love. </Text>
                    </View>
                </View>
            </View>
        )
    }
}

class Scroll extends Component {
    render(){
        return{

        }
    }
}

const styles = StyleSheet.create({
    body: {
        backgroundColor: 'whitesmoke'
    },
    top: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    bottom: {
        margin: 10,
        padding: 10,
        borderWidth: 0.5,
        borderColor: 'steelblue'
    },
    textBox: {
        marginTop: 35,
        height: 100,
        width: 170,
    },
    image: {
        borderRadius: 50,
        height: 100,
        width: 100
    },
    name: {
        fontSize: 20,
        color: 'steelblue'
    },
    title: {
        marginBottom: 10,
        fontSize: 20,
        fontWeight: 'bold'
    },
    basicText: {
        marginBottom: 15,
        fontSize: 15,
    }
});