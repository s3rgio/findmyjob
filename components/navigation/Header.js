import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight
} from 'react-native';

export default class Header extends Component {
  render() {
    return (
      <View style={styles.navigation}>

        <View style={styles.tabs}>
          <TouchableHighlight onPress={() => this.props.nav.push({index: 0})}>
            <Text style={styles.textTabs}> You </Text>
          </TouchableHighlight>
        </View>

        <View style={styles.tabs}>
          <TouchableHighlight onPress={() => this.props.nav.push({index: 1})}>
            <Text style={styles.textTabs}> Jobs </Text>
          </TouchableHighlight>
        </View>

        <View style={styles.tabs}>
          <TouchableHighlight onPress={() => this.props.nav.push({index: 2})}>
            <Text style={styles.textTabs}> About Us </Text>
          </TouchableHighlight>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  navigation: {
    paddingTop: 20,
    height: 55,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: '#F5FCFF',
  },
  tabs: {
    flex: 1,
    borderWidth: 0.5,
    borderColor: 'powderblue'
  },
  textTabs: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 5,
  }
});