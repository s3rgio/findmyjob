import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Navigator,
    TouchableHighlight
} from 'react-native';

import Header from "./components/navigation/Header";
import Profile from "./components/navigation/Profile";
import AboutUs from "./components/navigation/AboutUs";


class AwesomeProject extends Component {

    renderScene(route, nav) {

        let tabContent;

        switch (route.index){

            case 0:
                tabContent = <Profile/>;
                break;

            case 1:
                tabContent = <Profile/>;
                break;

            case 2:
                tabContent = <AboutUs/>;
                break;
        }

        return(
            <View>
                <Header nav={nav}/>

                {tabContent}

            </View>
        );
    }

    render() {
        const routes = [
            {title: 'First Scene', index: 0},
            {title: 'Second Scene', index: 1},
            {title: 'Second Scene', index: 2}
        ];
        return (

            <Navigator
                initialRoute={routes[0]}
                initialRouteStack={routes}
                renderScene={(route, navigator) =>

                    {return this.renderScene(route, navigator)}
                }
                configureScene={(route, routeStack) =>
                    Navigator.SceneConfigs.FloatFromLeft
                }
            />
        );
    }
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
    },
});

AppRegistry.registerComponent('AwesomeProject', () => AwesomeProject);